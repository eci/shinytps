#' generate_ShinyTP_tables
#'
#' This function generates a list the data tables used by the ShinyTPs app. These tables must then be saved in the global environment. Use the example below to generate the tables from the test data. Otherwise read in a data table with your own compounds using the read_shiny_data() function.
#'
#' @param cmpd A table containing  the compound name, SMILES and CIDs for which we want to find transformation reactions. This is generated with the read_shiny_data.R function.
#'
#' @return A list of tables to be used inside the ShinyTPs app
#' @importFrom dplyr mutate
#' @importFrom dplyr left_join
#' @importFrom dplyr select
#' @importFrom tidyr drop_na
#' @importFrom dplyr left_join
#' @importFrom tidyr separate_rows
#' @importFrom dplyr group_by
#' @importFrom dplyr ungroup
#' @importFrom dplyr row_number
#' @importFrom data.table as.data.table
#' @importFrom dplyr case_when
#' @importFrom dplyr cur_group_id
#' @importFrom dplyr "%>%"
#' @importFrom webchem pc_prop
#' @export
#'
#' @examples
#'  Table_list <- generate_ShinyTP_tables(ShinyTPs::Test_data_ZeroPM)
#' HSDB_text_table<- Table_list[[1]]
#' HSDB_table <- Table_list[[2]]
#' PubChem_table <- Table_list[[3]]
#' PubChem_table_TPs <- Table_list[[4]]
#' cmpd_info <- Table_list[[5]]
#' Reaction_table <- Table_list[[6]]
#' UIC_table <- Table_list[[7]]
#'
generate_ShinyTP_tables <- function(cmpd){
  PubChem_table <- ShinyTPs::PubChem_table%>%
    mutate(PCID = as.numeric(predecessorcid))%>%
    left_join(cmpd)%>%
    na.omit()%>%
    mutate(predecessor = INPUT)%>%
    select(-PCID, -SMILES, -INPUT)%>%
    mutate(predecessorcid = as.character(predecessorcid))%>%
    mutate(successorcid = as.character(successorcid))



  cmpd_info <- cmpd %>%
    left_join(Metabolism_Metabolites_HSDB_merged)

  cmpd_info <- cmpd_info%>%
    drop_na(desc_text)%>%
    select(INPUT, SMILES, PCID)%>%
    unique()




  PubChem_table_TPs <-  PubChem_table%>%
    select(predecessor,
           predecessorcid,
           successor,
           successorcid)%>%
    unique()
  selected_properties <- c("IsomericSMILES")

  if(length(cmpd_info$INPUT) == 0){
    HSDB_table = data.frame()%>%
      mutate(source_name = NA)%>%
      mutate(source_ID = NA)%>%
      mutate(source_chemical_name = NA)%>%
      mutate(cids = NA)%>%
      mutate(n_cids = NA)%>%
      mutate(all_cids = NA)%>%
      mutate(ref_text = NA)%>%
      mutate(desc_text = NA)%>%
      mutate(tp_cids = NA)%>%
      mutate(tp_start = NA)%>%
      mutate(part_1 = NA)%>%
      mutate(tp_text = NA)%>%
      mutate(part_1 = NA)%>%
      mutate(Parent_SMILES = NA)%>%
      mutate(TP_SMILES = NA)%>%
      mutate(entry = NA)


    HSDB_text_table <- ShinyTPs::Metabolism_Metabolites_HSDB_merged

    Reaction_table <- ShinyTPs::Metabolism_Metabolites_HSDB_merged %>%
      select(source_chemical_name,source_ID, cids, tp_cids, Parent_SMILES, TP_SMILES,tp_text, entry, ref_text)%>%
      mutate(tp_cids = as.character(tp_cids))%>%
      mutate(source_ID = as.character(source_ID))%>%
      mutate(cids = as.character(cids))%>%
      unique()%>%
      mutate(enzyme = "NA")%>%
      mutate(biosystem = "NA")%>%
      mutate(transformation = "Metabolism")%>%
      mutate(Source = "HSDB")%>%
      mutate(`Source_Description` = "HSDB is a toxicology database that focuses on the toxicology of potentially hazardous chemicals. See https://pubchem.ncbi.nlm.nih.gov/source/11933")%>%
      mutate(`evidence DOI` = "https://doi.org/10.5281/zenodo.3827487")%>%
      mutate(DescReactionSMILES = paste(Parent_SMILES, ">[",transformation, "]>", TP_SMILES, " CID: ", cids, " to CID: ", tp_cids, sep = ""))


  }else {
    HSDB_table <- ShinyTPs::Metabolism_Metabolites_HSDB_merged%>%
      left_join(cmpd_info)%>%
      mutate(source_chemical_name = INPUT)%>%
      drop_na(SMILES)%>%
      select(source_name, source_ID, source_chemical_name, cids, n_cids, all_cids, ref_text, desc_text, tp_cids, tp_start, tp_length)%>%
      separate_rows(tp_cids, tp_start, tp_length, sep = ";", convert = FALSE)%>%
      mutate(tp_cids = as.numeric(tp_cids))%>%
      drop_na(tp_cids)%>%
      mutate(tp_start = as.numeric(tp_start))%>%
      mutate(tp_length = as.numeric(tp_length))%>%
      mutate(tp_end = tp_start + tp_length)%>%
      mutate(part_1 = substr(desc_text, start = 1, stop = (tp_start)))%>%
      mutate(tp_text = substr(desc_text, start = (tp_start+1), stop = tp_end))%>%
      mutate(part_2 = substr(desc_text, start = (tp_end +1), stop = nchar(desc_text)))


    # retrieve info with webchem
    CID_info_all <- as.data.table(webchem::pc_prop(HSDB_table$cids, selected_properties))%>%
      mutate(cids = CID)%>%
      mutate(Parent_SMILES = IsomericSMILES)%>%
      select(cids, Parent_SMILES)

    HSDB_table <- HSDB_table %>%
      left_join(CID_info_all)%>%
      unique()


    CID_info_all <- as.data.table(webchem::pc_prop(HSDB_table$tp_cids, selected_properties))%>%
      mutate(tp_cids = CID)%>%
      mutate(TP_SMILES = IsomericSMILES)%>%
      select(tp_cids, TP_SMILES)

    HSDB_table <- HSDB_table %>%
      left_join(CID_info_all)
    HSDB_table <- HSDB_table %>%
      unique()%>%
      mutate(Parent_TP_SMILES = paste(Parent_SMILES,TP_SMILES, sep = "."))%>%
      unique()%>%
      group_by(Parent_TP_SMILES)%>%
      mutate(entry = row_number())%>%
      ungroup()%>%
      select(-Parent_TP_SMILES)

    Reaction_table <- HSDB_table %>%
      select(source_chemical_name,source_ID, cids, tp_cids, Parent_SMILES, TP_SMILES, tp_text, entry, ref_text)%>%
      mutate(tp_cids = as.character(tp_cids))%>%
      mutate(source_ID = as.character(source_ID))%>%
      mutate(cids = as.character(cids))%>%
      unique()%>%
      mutate(enzyme = "NA")%>%
      mutate(biosystem = "NA")%>%
      mutate(transformation = "Metabolism")%>%
      mutate(Source = "HSDB")%>%
      mutate(`Source_Description` = "HSDB is a toxicology database that focuses on the toxicology of potentially hazardous chemicals. See https://pubchem.ncbi.nlm.nih.gov/source/11933")%>%
      mutate(`evidence DOI` = case_when(substr(ref_text, 1, 4) == "PMID" ~ ref_text,
                                        substr(ref_text, 1, 3) == "DOI" ~ ref_text,
                                        TRUE ~ "https://doi.org/10.5281/zenodo.3827487"))%>%
      mutate(DescReactionSMILES = paste(Parent_SMILES, ">[",transformation, "]>", TP_SMILES, " CID: ", cids, " to CID: ", tp_cids, sep = ""))

    Titles <- pc_prop(Reaction_table$tp_cids, "Title")

    Reaction_table <- Reaction_table%>%
      cbind(Titles)%>%
      mutate(tp_text = Title)%>%
      select(-Title, -CID)%>%
      filter(cids != tp_cids)

    HSDB_text_table <- HSDB_table %>%
      group_by(cids, desc_text)%>%
      mutate(entry = cur_group_id())%>%
      ungroup()}

  UIC_table <- HSDB_text_table   %>%
    select(source_chemical_name, source_ID, cids, Parent_SMILES, entry, ref_text)%>%
    mutate(enzyme = "NA")%>%
    mutate(biosystem = "NA")%>%
    mutate(transformation = "Metabolism")%>%
    mutate(Source = "HSDB")%>%
    mutate(`Source_Description` = "HSDB is a toxicology database that focuses on the toxicology of potentially hazardous chemicals. See https://pubchem.ncbi.nlm.nih.gov/source/11933")%>%
    mutate(`evidence DOI` = case_when(substr(ref_text, 1, 4) == "PMID" ~ ref_text,
                                      substr(ref_text, 1, 3) == "DOI" ~ ref_text,
                                      TRUE ~ "https://doi.org/10.5281/zenodo.3827487"))%>%
    unique()

  return(list(HSDB_text_table,HSDB_table,PubChem_table, PubChem_table_TPs, cmpd_info, Reaction_table, UIC_table))

}
