#' Run_ShinyTPs
#'
#' ShinyTPs is a tool for curating transformation reactions from the PubChem Metabolism and Metabolites section based on the textmining done through LeadMine. For further information on how to use ShinyTPs please see the ShinyTPs package documentation
#'
#' @param HSDB_text_table The first table from the generate_ShinyTP_tables function
#' @param HSDB_table The second table from the generate_ShinyTP_tables function
#' @param PubChem_table The third table from the generate_ShinyTP_tables function
#' @param PubChem_table_TPs The fourth table from the generate_ShinyTP_tables function
#' @param cmpd_info The fifth table from the generate_ShinyTP_tables function
#' @param Reaction_table The sixth table from the generate_ShinyTP_tables function
#' @param UIC_table The seventh table from the generate_ShinyTP_tables function
#'
#' @return Starting the ShinyTPs app
#' @importFrom shiny fluidPage
#' @importFrom shiny titlePanel
#' @importFrom shiny navbarPage
#' @importFrom shiny tabPanel
#' @importFrom shiny mainPanel
#' @importFrom shiny h1
#' @importFrom shiny h2
#' @importFrom shiny h3
#' @importFrom shiny h4
#' @importFrom shiny h5
#' @importFrom shiny h6
#' @importFrom shiny tableOutput
#' @importFrom shiny imageOutput
#' @importFrom shiny downloadButton
#' @importFrom shiny sidebarLayout
#' @importFrom shiny sidebarPanel
#' @importFrom shiny selectInput
#' @importFrom shiny textInput
#' @importFrom shiny fluidRow
#' @importFrom shiny splitLayout
#' @importFrom shiny p
#' @importFrom shiny textOutput
#' @importFrom shiny strong
#' @importFrom shiny span
#' @importFrom shiny actionButton
#' @importFrom miniUI miniTitleBar
#' @importFrom miniUI miniTitleBarButton
#' @importFrom bslib bs_theme
#' @importFrom bslib font_google
#' @importFrom shiny tags
#' @importFrom shiny column
#' @importFrom shiny HTML
#' @importFrom shiny renderImage
#' @importFrom shiny renderTable
#' @importFrom dplyr filter
#' @importFrom shiny downloadHandler
#' @importFrom shiny observe
#' @importFrom dplyr select
#' @importFrom shiny updateSelectInput
#' @importFrom crosstalk getDefaultReactiveDomain
#' @importFrom shiny reactive
#' @importFrom dplyr mutate
#' @importFrom dplyr case_when
#' @importFrom dplyr select
#' @importFrom shiny reactiveValues
#' @importFrom shiny observeEvent
#' @importFrom DT renderDataTable
#' @importFrom shiny renderText
#' @importFrom shiny shinyApp
#' @importFrom shiny headerPanel
#' @importFrom tibble tibble
#' @importFrom chemdoodle processChemDoodleJson
#' @importFrom DT dataTableOutput
#' @importFrom chemdoodle toSmiles
#' @importFrom chemdoodle renderChemdoodle
#' @importFrom chemdoodle chemdoodle_viewer
#' @importFrom chemdoodle chemdoodle_viewerOutput
#' @importFrom chemdoodle chemdoodle_sketcher
#' @importFrom shiny radioButtons
#' @import chemdoodle

#'
#' @export
#'
#' @examples
#' Cmpd <- ShinyTPs::Test_data_ZeroPM
#'
#' Table_list <- generate_ShinyTP_tables(cmpd = Cmpd)
#'
#' HSDB_text_table<- Table_list[[1]]
#' HSDB_table <- Table_list[[2]]
#' PubChem_table <- Table_list[[3]]
#' PubChem_table_TPs <- Table_list[[4]]
#' cmpd_info <- Table_list[[5]]
#' Reaction_table <- Table_list[[6]]
#' UIC_table <- Table_list[[7]]
#'
#' Run_ShinyTPs(HSDB_text_table, HSDB_table, PubChem_table, PubChem_table_TPs, cmpd_info, Reaction_table, UIC_table)
Run_ShinyTPs <- function(HSDB_text_table, HSDB_table, PubChem_table, PubChem_table_TPs, cmpd_info, Reaction_table, UIC_table) {

  shinyApp(
    ui <- fluidPage(
      #titlePanel(fluidRow(column(fluidRow(column(h1("ShinyTPs"), width = 2)), width = 5))),

      #theme = shinytheme("flatly"),
      theme = bs_theme(
        bootswatch = "united", #We choose this one because otherwise the Nav bar looks strange or the font colour looks too light
        bg = "#FFFFFF",
        fg = "#2E2D4D",
        primary = "#2E2D4D",
        secondary = "#F79A4A",
        success = "#F79A4A",
        base_font = font_google("Work Sans"),
        code_font = font_google("Space Mono"),

      ),
      navbarPage(title = "ShinyTPs",
                 if(length(cmpd_info$SMILES) == 0){
                   tabPanel(title = "About",
                            mainPanel(
                              h1("There is no available reaction information for the compounds in the selected file")))
                 }else{
                   tabPanel(title = "About",
                            mainPanel(
                              h2("Welcome to ShinyTPs"),
                              h5("The main section of this app deals with curating transformation reactions from the Metabolism and Metabolites section of PubChem which contains of text-based information about transformation reactions and related experiments. PubChem runs LeadMine to identify compound names in this text and link them to the corresponding entry in PubChem. This app uses this extracted information to allow the user to easily go through the identified compound names determine if they are transformation products or parent compounds of the input compound. The reactions can be easily saved for further use and for upload to PubChem to expand the transformations library. In addition, the user can also save the reactions that are not identified by the text mining software. Below is a description of the different tabs in this app however, for a more in-depth description of how to use ShinyTPs please see the documentation available on GitLab https://gitlab.lcsb.uni.lu/eci/shinytps."),
                              h4("PubChem Transformations"),
                              h5("Here you will find all reactions from the reaction tables in PubChem for the compounds listed in the input file. You can save the table with the download button"),
                              h4("Transformation reaction curation"),
                              h5("Here you can go through the chemical names identified by LeadMine to investigate if they represent valid precursors or transformation products to the input compounds. As LeadMine extracts all types chemical names not just those related to the transformation reaction this validation is important to make sure no incorrect reactions are curated."),
                              h4("Adding missing entries"),
                              h5("In some cases the chemical names in the text cannot be identified by LeadMine due to them missing from the Pubchem Synonyms dictionary. In this case these reactions can be curated in the Adding missing entries tab which allows for manually obtaining the SMILES for the compound using the drawing tool."),
                            ))},




                 tabPanel(title = "PubChem Transformations",
                          sidebarLayout(
                            sidebarPanel(
                              selectInput("Parent", "Select a compound", choices = PubChem_table$predecessor, multiple = TRUE),
                              downloadButton("downloadDataPubChem", "Download")
                            ),
                            mainPanel(
                              tableOutput("PubChem_TPs")
                            )
                          )
                 ),

                 # tabPanel(title = "Transformations by reaction type",
                 #          sidebarLayout(
                 #            sidebarPanel(
                 #              selectInput("Trans", "Select the transformation type", choices = PubChem_table$transformation)
                 #            ),
                 #            mainPanel(
                 #              tableOutput("TP_types")
                 #            )
                 #          )
                 # ),

                 # tabPanel(title = "Biochemical reactions",
                 #          # sidebarLayout(
                 #          #   sidebarPanel(
                 #          #     selectInput("ParentCID_BC", "Select a compound", choices = cmpd_info$PCID)
                 #          #   ),
                 #            mainPanel(
                 #              tableOutput("BC_Table")
                 #            )
                 #          ),
                 #

                 tabPanel(title = "Transformation reaction curration",
                          sidebarLayout(
                            sidebarPanel(
                              selectInput("Parent_selector", "Select a input compound", choices = Reaction_table$source_chemical_name),
                              selectInput("TP_selector", "Select a transformation product CID", choices = NULL),
                              selectInput("Entry_selector", "Select an entry", choices = NULL),
                              #uiOutput("secondSelection")
                              textInput(inputId = 'Transformation',
                                        label ='Please add the transformation type if specified in the text',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              textInput(inputId = 'enzyme',
                                        label ='Please add the enzyme if specified in the text',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              textInput(inputId = 'biosystem',
                                        label ='Please add the biosystem if specified in the text',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              radioButtons(inputId = 'Parent_TP',
                                          label ='Is the compound a precursor or TP?',
                                          choices = c("Precursor", "Transformation product"),
                                          selected = "Transformation product"),
                              textInput(inputId = 'comment',
                                        label ='Comment',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              width = 3,
                              position = "left"

                            ),
                            mainPanel(
                              tableOutput("Selected_reaction"),
                              fluidRow(
                                splitLayout(cellWidths = c("50%", "50%"), h4("Input compound structure"), h4("Potential parent or TP structure"))
                              ),
                              fluidRow(
                                splitLayout(cellWidths = c("50%", "50%"), chemdoodle_viewerOutput(outputId = "Parent_image", height = 300, width = 300), chemdoodle_viewerOutput(outputId = "TP_image", height = 300, width = 300))
                              ),
                              h3("Text in HSDB:"),
                              h4(textOutput("HSDB_Text_1", inline = TRUE),strong(span(textOutput("HSDB_Text_TP", inline = TRUE), style = "color:#008BD5")),textOutput("HSDB_Text_2", inline = TRUE)),
                              h3("Selected Reactions"),
                              fluidRow(actionButton("submit_2", "Add to table")),
                              fluidRow(DT::dataTableOutput("Reaction_List")),
                              downloadButton("downloadDataHSDB", "Download")
                            )

                          )),

                 tabPanel(title = "Adding missing entries",
                          sidebarLayout(
                            sidebarPanel(
                              selectInput("Parent_selector_HSDB", "Select a input compound", choices = HSDB_text_table$source_chemical_name),
                              selectInput("Entry_selector_HSDB", "Select an entry", choices = NULL),
                              textInput("Compound", "Compound", ""),
                              textInput("SMILES", "SMILES input", ""),
                              textInput(inputId = 'Transformation_UIC',
                                        label ='Please add the transformation type if specified in the text',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              textInput(inputId = 'enzyme_UIC',
                                        label ='Please add the enzyme if specified in the text',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              textInput(inputId = 'biosystem_UIC',
                                        label ='Please add the biosystem if specified in the text',
                                        value = "", width = NULL,
                                        placeholder = NULL),
                              radioButtons(inputId = 'Parent_TP_UIC',
                                          label ='Is the compound a precursor or TP?',
                                          choices = c("Precursor", "Transformation product"),
                                          selected = "Transformation product"),
                              textInput(inputId = 'comment_UIC',
                                        label ='Comment',
                                        value = "", width = NULL,
                                        placeholder = NULL),



                            ),
                            mainPanel(
                              tags$head(
                                tags$style(HTML("
mark {
  padding: 0;
  background-color: white;
  color: #F79A4A;
  font-weight: bold;
}
mark_2{
  padding: 0;
  background-color: white;
  color: #F6A2C9;
  font-weight: bold;
}
"))),
                              #HTML(paste(textOutput("output")))
                              p(id = "test", tableOutput("output")),
                              fluidRow(fluidRow(h4(textOutput("smiles")))),
                              fluidRow(column(6,
                                              fluidRow(chemdoodle_sketcher(mol=NULL)),
                                              fluidRow(miniTitleBar("Draw A Molecule", right = miniTitleBarButton("done", "Done", primary = TRUE))))),
                              tableOutput("UIC"),
                              fluidRow(column(6,
                                              headerPanel(""),
                                              fluidRow(actionButton("submit", "Add to table")),
                                              fluidRow(DT::dataTableOutput("thelist")),
                                              fluidRow(downloadButton("downloadDataNames", "Download")))),

                              tags$script('
              document.getElementById("done").onclick = function() {
              var mol = sketcher.getMolecule();
              var jsonmol = new ChemDoodle.io.JSONInterpreter().molTo(mol);
              Shiny.onInputChange("moleculedata", jsonmol);};'
                              )

                            )

                          ))
      )),


    server <- function(input, output) {


      output$PubChem_TPs <- renderTable({
        PubChem_table%>%
          filter(predecessor %in% input$Parent)
      })

      output$downloadDataPubChem <- downloadHandler(
        filename = function(){
          paste("PubChem_download", ".csv", sep = "")

        },

        content = function(file){
          write.csv((PubChem_table%>%
                      filter(predecessor %in% input$Parent)), file)
        }
      )





      observe({
        print(input$Parent_selector)
        x <- Reaction_table %>% filter(source_chemical_name == input$Parent_selector)%>% select(tp_cids)
        updateSelectInput(session = getDefaultReactiveDomain(), "TP_selector", "Select  a transformation product CID", choices = x) #This updates the select input function. It took you like three days to solve this so we DO NOT mess with it any more!!!!
      })

      observe({
        y <- Reaction_table %>% filter(source_chemical_name == input$Parent_selector, tp_cids == input$TP_selector)%>% select(entry)
        updateSelectInput(session = getDefaultReactiveDomain(), "Entry_selector", "Select an entry", choices = y)
      })


      ReactiveReactionTable <- reactive(
        Reaction_table%>%
          filter(source_chemical_name == input$Parent_selector)%>%
          filter(tp_cids == input$TP_selector)%>%
          filter(entry == input$Entry_selector)%>%
          mutate(enzyme = case_when(input$enzyme != "" ~ input$enzyme,
                                    TRUE ~ "NA"))%>%
          mutate(transformation = case_when(input$Transformation != "" ~ input$Transformation,
                                            TRUE ~ "Metabolism"))%>%
          mutate(biosystem = case_when(input$biosystem != "" ~ input$biosystem,
                                       TRUE ~ "NA"))%>%
          mutate(Comment = case_when(input$comment != "" ~ input$comment,
                                     TRUE ~ "NA"))%>%
          mutate(PorT = input$Parent_TP)%>%
          mutate(Input_compound = source_chemical_name)%>%
          mutate(LeadMine_CID = tp_cids)%>%
          mutate(Input_SMILES = Parent_SMILES)%>%
          mutate(LeadMine_SMILES = TP_SMILES)%>%
          mutate(LeadMine_compound_text = tp_text)%>%
          select(Input_compound,
                 source_ID, cids,
                 LeadMine_CID,
                 Input_SMILES,
                 LeadMine_SMILES,
                 LeadMine_compound_text,
                 entry,
                 ref_text,
                 enzyme,
                 biosystem,
                 transformation,
                 Source,
                 Source_Description,
                 `evidence DOI`,
                 DescReactionSMILES,
                 Comment))

      ReactiveHSDBTable <- reactive(
        HSDB_table%>%
          filter(source_chemical_name == input$Parent_selector)%>%
          filter(tp_cids == input$TP_selector)%>%
          filter(entry == input$Entry_selector)
      )


      output$Selected_reaction <- renderTable({
        ReactiveReactionTable()
      })




      HSDBTableForUpload <- reactive(
        Reaction_table%>%
          filter(source_chemical_name == input$Parent_selector)%>%
          filter(tp_cids == input$TP_selector)%>%
          filter(entry == input$Entry_selector)%>%
          mutate(Dataset_DOI = "10.5281/zenodo.3827487")%>%
          mutate(Dataset_Description = "Transformation Products extracted from HSDB content in PubChem, validated by LCSB-ECI")%>%
          mutate(Evidence_ID = "")%>%
          mutate(Evidence_Description = ref_text)%>%
          mutate(Enzyme = case_when(input$enzyme != "" ~ input$enzyme,
                                    TRUE ~ "NA"))%>%
          mutate(Comment = case_when(input$comment != "" ~ input$comment,
                                     TRUE ~ "NA"))%>%
          mutate(Transformation = case_when(input$Transformation != "" ~ input$Transformation,
                                            TRUE ~ "Metabolism"))%>%
          mutate(Biosystem = case_when(input$biosystem != "" ~ input$biosystem,
                                       TRUE ~ "NA"))%>%
          mutate(PorT = input$Parent_TP)%>%
          mutate(PCID = case_when(PorT == "Precursor" ~ tp_cids,
                                  TRUE ~ cids))%>%
          mutate(TPCID = case_when(PorT == "Precursor" ~ cids,
                                   TRUE ~ tp_cids))%>%
          mutate(Predecessor_CID = PCID)%>%
          mutate(Successor_CID = TPCID)%>%
          mutate(PName = case_when(PorT == "Transformation product" ~ source_chemical_name,
                                   TRUE ~ tp_text))%>%
          mutate(TPName = case_when(PorT == "Transformation product" ~ tp_text,
                                    TRUE ~ source_chemical_name))%>%
          mutate(Predecessor_Name = PName)%>%
          mutate(Successor_Name = TPName)%>%
          mutate(PSMILES = case_when(PorT == "Transformation product" ~ Parent_SMILES,
                                     TRUE ~ TP_SMILES))%>%
          mutate(TPSMILES = case_when(PorT == "Transformation product" ~ TP_SMILES,
                                      TRUE ~ Parent_SMILES))%>%
          mutate(Parent_SMILES = PSMILES)%>%
          mutate(TP_SMILES = TPSMILES)%>%
          mutate(DescReactionSMILES = paste(Parent_SMILES, ">[",transformation, "]>", TP_SMILES, " CID: ", Predecessor_CID, " to CID: ", Successor_CID, sep = ""))%>%
          select(Predecessor_CID,
                 Predecessor_Name,
                 Successor_CID,
                 Successor_Name,
                 Transformation,
                 Biosystem,
                 Enzyme,
                 source_ID,
                 Source,
                 Source_Description,
                 Dataset_DOI,
                 Dataset_Description,
                 Evidence_ID,
                 Evidence_Description,
                 DescReactionSMILES,
                 Comment))



      Reaction_List <- reactiveValues(data = data.frame(source_chemical_name = character(),
                                                        cids = numeric(),
                                                        tp_cids = numeric(),
                                                        Parent_SMILES = character(),
                                                        TP_SMILES = character(),
                                                        tp_text = character(),
                                                        entry = numeric(),
                                                        enzyme = character(),
                                                        biosystem = character(),
                                                        Transformation = character(),
                                                        Source = character(),
                                                        `Source Description` = character(),
                                                        `evidence DOI` = character(),
                                                        DescReactionSMILES = character(),
                                                        Comment = character()))

      Reaction_ListHSDB <- reactiveValues(data = data.frame(Predecessor_CID = character(),
                                                            Predecessor_Name = character(),
                                                            Successor_CID = character(),
                                                            Successor_Name = character(),
                                                            Transformation = character(),
                                                            Biosystem = character(),
                                                            Enzyme = character(),
                                                            Source_ID = character(),
                                                            Source = character(),
                                                            Source_Description = character(),
                                                            Dataset_DOI = character(),
                                                            Dataset_Description = character(),
                                                            Evidence_ID = character(),
                                                            Evidence_Description = character(),
                                                            Comment = character()))


      observeEvent(input$submit_2,{
        Reaction_List$data <- rbind(Reaction_List$data, ReactiveReactionTable())
        Reaction_ListHSDB$data = rbind(Reaction_ListHSDB$data, HSDBTableForUpload())
      })

      output$Reaction_List = DT::renderDataTable({
        Reaction_ListHSDB$data
      })

      # output$downloadData <- downloadHandler(
      #   filename = function(){
      #     paste("HSDB.csv", sep = "")
      #
      #   },
      #
      #   content = function(file){
      #     write.csv(Reaction_List$data, file, row.names = FALSE)
      #   },
      # contentType = "text/csv"
      # )

      Reaction_ListHSDB_download <- reactive(Reaction_ListHSDB$data)

      output$downloadDataHSDB <- downloadHandler(
        filename = function(){
          paste("HSDB_Zenodo.csv", sep = "")

        },

        content = function(file){
          write.csv(Reaction_ListHSDB_download(), file, row.names = FALSE)
        },
        contentType = "text/csv"
      )# If you want your file saved as a csv file add ".csv"to the file name after pressing the download button. Otherwise it will save it as a generic file without the extension.










      output$Parent_image <- renderChemdoodle(chemdoodle_viewer(ReactiveHSDBTable()$Parent_SMILES,
                                                                width = 300,
                                                                height = 300,
                                                                atoms_font_size_2D = 14,
                                                                bondscale = 25,
                                                                bonds_width_2D = 2))
      output$TP_image <- renderChemdoodle(chemdoodle_viewer(ReactiveHSDBTable()$TP_SMILES,
                                                            width = 300,
                                                            height = 300,
                                                            atoms_font_size_2D = 14,
                                                            bondscale = 25,
                                                            bonds_width_2D = 2))


      output$HSDB_Text_1 <- renderText({
        ReactiveHSDBTable()$part_1
      })

      output$HSDB_Text_TP <- renderText({
        ReactiveHSDBTable()$tp_text
      })


      output$HSDB_Text_2 <- renderText({
        ReactiveHSDBTable()$part_2
      })

      # output$downloadData <- downloadHandler(
      #   filename = function(){
      #     paste("HSDB_download", input$Reaction, ".csv", sep = "")
      #
      #   },
      #
      #   content = function(file){
      #     write.csv(ReactiveReactionTable(), file)
      #   }
      # )


      observe({
        y <- HSDB_text_table$entry[HSDB_text_table$source_chemical_name == input$Parent_selector_HSDB]
        updateSelectInput(session = getDefaultReactiveDomain(), "Entry_selector_HSDB", "Select an entry", choices = y)
      })

      ReactiveHSDBTable_text <- reactive(
        HSDB_text_table%>%
          filter(source_chemical_name == input$Parent_selector_HSDB)%>%
          filter(entry == input$Entry_selector_HSDB)
      )


      highligthed <- reactive({
        case_when(ReactiveHSDBTable_text()$cids == ReactiveHSDBTable_text()$tp_cids ~
                    gsub(paste0("(", paste(unlist(as.list(ReactiveHSDBTable_text()$tp_text)), collapse = "|"), ")"), "<mark>\\1</mark>", ReactiveHSDBTable_text()$desc_text),
                  ReactiveHSDBTable_text()$cids != ReactiveHSDBTable_text()$tp_cids ~
                    gsub(paste0("(", paste(unlist(as.list(ReactiveHSDBTable_text()$tp_text)), collapse = "|"), ")"), "<mark>\\1</mark>", ReactiveHSDBTable_text()$desc_text))
      })


      df_reactive = reactive({
        tibble(text = highligthed())
      })

      # output$output = renderText({df_reactive()$text[1]})

      output$output = renderTable({
        df_reactive()$text[1]
      }, sanitize.text = function(x) x)


      mol <- reactiveValues(moleculedata = NULL)

      #function to update the value based on changes on the shiny side
      observeEvent(input$moleculedata, {
        moljson <- input$moleculedata
        mol$moleculedata <- processChemDoodleJson(moljson)

      })

      # output function simply tallies the atom counts
      output$smiles <- renderText({
        if (is.null(mol$moleculedata)){
          return("Choose a Molecule and Click the Button!")
        } else {
          smiles <- toSmiles(mol$moleculedata)

          return(paste("Smiles:", smiles))
        }
      })

      ReactiveUIC_table <- reactive(
        UIC_table%>%
          filter(source_chemical_name == input$Parent_selector_HSDB)%>%
          filter(entry == input$Entry_selector_HSDB)%>%
          mutate(cids = as.character(cids))%>%
          mutate(Dataset_DOI = "10.5281/zenodo.3827487")%>%
          mutate(Dataset_Description = "Transformation Products extracted from HSDB content in PubChem, validated by LCSB-ECI")%>%
          mutate(Evidence_ID = "")%>%
          mutate(Evidence_Description = ref_text)%>%
          mutate(Enzyme = case_when(input$enzyme_UIC != "" ~ input$enzyme_UIC,
                                    TRUE ~ "NA"))%>%
          mutate(Comment = case_when(input$comment_UIC != "" ~ input$comment_UIC,
                                     TRUE ~ "NA"))%>%
          mutate(Transformation = case_when(input$Transformation_UIC != "" ~ input$Transformation_UIC,
                                            TRUE ~ "Metabolism"))%>%
          mutate(Biosystem = case_when(input$biosystem_UIC != "" ~ input$biosystem_UIC,
                                       TRUE ~ "NA"))%>%
          mutate(PorT = input$Parent_TP_UIC)%>%
          mutate(Predecessor_CID = case_when(PorT == "Precursor" ~ "NA",
                                             TRUE ~ cids))%>%
          mutate(Successor_CID = case_when(PorT == "Precursor" ~ cids,
                                           TRUE ~ "NA"))%>%
          mutate(Predecessor_Name = case_when(PorT == "Precursor" ~ input$Compound,
                                              TRUE ~ input$Parent_selector_HSDB))%>%
          mutate(Successor_Name = case_when(PorT == "Precursor" ~ input$Parent_selector_HSDB,
                                            TRUE ~ input$Compound))%>%
          mutate(PSMILES = case_when(PorT == "Precursor" ~ input$SMILES,
                                     TRUE ~ Parent_SMILES))%>%
          mutate(TPSMILES = case_when(PorT == "Precursor" ~ Parent_SMILES,
                                      TRUE ~ input$SMILES))%>%
          mutate(DescReactionSMILES = paste(PSMILES, ">[",transformation, "]>", TPSMILES, " CID: ", Predecessor_CID, " to CID: ", Successor_CID, sep = ""))%>%

          select(Predecessor_CID,
                 Predecessor_Name,
                 PSMILES,
                 Successor_CID,
                 Successor_Name,
                 TPSMILES,
                 Transformation,
                 Biosystem,
                 Enzyme,
                 source_ID,
                 Source,
                 Source_Description,
                 Dataset_DOI,
                 Dataset_Description,
                 Evidence_ID,
                 Evidence_Description,
                 DescReactionSMILES,
                 Comment))


      output$UIC <- renderTable({
        ReactiveUIC_table()
      })

      download_UIC <- reactiveValues(data = data.frame(Predecessor_CID = character(),
                                                       Predecessor_Name = character(),
                                                       PSMILES = character(),
                                                       Successor_CID = character(),
                                                       Successor_Name = character(),
                                                       TPSMILES = character(),
                                                       Transformation = character(),
                                                       Biosystem = character(),
                                                       Enzyme = character(),
                                                       source_ID = character(),
                                                       Source = character(),
                                                       Source_Description = character(),
                                                       Dataset_DOI = character(),
                                                       Dataset_Description = character(),
                                                       Evidence_ID = character(),
                                                       Evidence_Description = character(),
                                                       DescReactionSMILES = character(),
                                                       Comment = character()))

      observeEvent(input$submit,{
        download_UIC$data <- rbind(download_UIC$data, ReactiveUIC_table())
      })


      download_UIC_2 <- reactive(download_UIC$data)


      output$thelist = DT::renderDataTable({
        download_UIC$data
      })
      output$downloadDataNames <- downloadHandler(
        filename = function(){
          paste("Missed_compounds", ".csv", sep = "")

        },

        content = function(file){
          write.csv(download_UIC_2(), file)
        }
      )



    }

  )}
