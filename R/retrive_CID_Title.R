
#' retrive_CID_Title
#'
#' This function will retrieve the CIDs and PubChem titles for the compounds in the file downloaded from the expert curation in ShinyTPs.
#'
#' @param file the CSV file directory saved from the expert curation in ShinyTPs
#'
#' @return The input table with CIDs and titles for the compounds which have PubChem entries
#' @export
#'
#' @importFrom dplyr mutate
#' @importFrom dplyr select
#' @importFrom dplyr "%>%"
#' @importFrom readr read_delim
#' @importFrom dplyr left_join
#' @importFrom webchem get_cid
#' @importFrom dplyr case_when
#' @importFrom webchem pc_prop
#'
#'
#' @examples
#' retrive_CID_Title(file = "my file directory")
#'
#'@keywords internal
retrive_CID_Title <- function(file) {
  Reactions <- read_delim(file = file, col_names = TRUE, delim = ",")

  SCID <- get_cid(Reactions$TPSMILES, from = "smiles")%>%
    mutate(cid = case_when(cid == 0 ~ NA,
                           TRUE ~ cid))%>%
    mutate(TPSMILES = query)%>%
    select(cid, TPSMILES)

  Reactions <- Reactions %>%
    left_join(SCID)%>%
    unique()%>%
    mutate(Successor_CID = case_when(is.na(Successor_CID) == TRUE ~ cid,
                                     TRUE ~ as.character(Successor_CID)))%>%
    select(-cid)

  PCID <- get_cid(Reactions$PSMILES, from = "smiles")%>%
    mutate(cid = case_when(cid == 0 ~ NA,
                           TRUE ~ cid))%>%
    mutate(PSMILES = query)%>%
    select(cid, PSMILES)

  Reactions <- Reactions %>%
    left_join(PCID)%>%
    unique()%>%
    mutate(Predecessor_CID = case_when(is.na(Predecessor_CID) == TRUE ~ cid,
                                       TRUE ~ as.character(Predecessor_CID)))

  Titles <- pc_prop(Reactions$Predecessor_CID, "Title")%>%
    mutate(Predecessor_CID = as.character(CID))%>%
    mutate(Predecessor_title = Title)%>%
    select(Predecessor_CID, Predecessor_title)

  Reactions <- Reactions %>%
    left_join(Titles)%>%
    unique()

  Titles <- pc_prop(Reactions$Successor_CID, "Title")%>%
    mutate(Successor_CID = as.character(CID))%>%
    mutate(Successor_title = Title)%>%
    select(Successor_CID, Successor_title)

  Reactions <- Reactions %>%
    left_join(Titles)%>%
    unique()

  return(Reactions)
}
